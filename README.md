# README #

Это pinescript для Tradinvgview

## Williams Alligator Импульс ##
Разметка на поиск Импульса.
Кратко: Я прыгаю на каждый импульс, но по итогу это может оказаться не импульсом, а сном. По итогу я часто вхожу, но рано или поздно оседлаю импульс.
Стратегия показала что оптимально работает на 4H
на мелких ТФ - заходит часто. НО комиссия становится огромной из-за частоты сделок.
На больших ТФ заходит очень редко.
Еще обращаю внимания что вхожу на пересечении короткой и длинной. Среднюю не трогаю. По ней я делал анализ, эффективность плохая.

Оптимально: 4Х ~ +30%  
```
//@version=4
//study(title="Williams Alligator IMPULSE", shorttitle="Alligator", overlay=true, resolution="")
strategy("Williams Alligator IMPULSE", overlay=true)

startDate = input(title="Start Date", type=input.integer,
     defval=1, minval=1, maxval=31)
startMonth = input(title="Start Month", type=input.integer,
     defval=1, minval=1, maxval=12)
startYear = input(title="Start Year", type=input.integer,
     defval=2021, minval=1800, maxval=2100)

endDate = input(title="End Date", type=input.integer,
     defval=1, minval=1, maxval=31)
endMonth = input(title="End Month", type=input.integer,
     defval=7, minval=1, maxval=12)
endYear = input(title="End Year", type=input.integer,
     defval=2021, minval=1800, maxval=2100)
     
inDateRange = (time >= timestamp(syminfo.timezone, startYear,
         startMonth, startDate, 0, 0)) and
     (time < timestamp(syminfo.timezone, endYear, endMonth, endDate, 0, 0))

     
smma(src, length) =>
    smma =  0.0
    smma := na(smma[1]) ? sma(src, length) : (smma[1] * (length - 1) + src) / length
    smma

jawLength = input(13, minval=1, title="Jaw Length")
lipsLength = input(5, minval=1, title="Lips Length")

jawOffset = input(8, title="Jaw Offset")
lipsOffset = input(3, title="Lips Offset")

jaw = smma(hl2, jawLength)
lips = smma(hl2, lipsLength)

longCondition = crossover(lips, jaw)
shortCondition = crossunder(lips, jaw)


plotshape((inDateRange and longCondition), style=shape.triangleup)
plotshape((inDateRange and shortCondition), style=shape.triangledown)

if (inDateRange and longCondition)
    strategy.entry("My Long Entry Id", strategy.long)
if (inDateRange and shortCondition)
    strategy.entry("My Short Entry Id", strategy.short)

plot(jaw, "Jaw", offset = jawOffset, color=#3BB3E4)
plot(lips, "Lips", offset = lipsOffset, color=#36C711)
```
### Williams Alligator СОН ###
Просто РЕВЕРС импульса, те все делать наоборот. Эта стратегия работает только когда крокодил спит.
НО беда, что импульс намного жирнее по пунктам, чем профит, который я собираю, когда крокодил спит. Поэтому стратегия показала низкую эффективность.



### Fibonacchi ###
BTCUSDT 5m
Profit = F382
Net profit = 3.82%
Trades = 58
Profittable = 77%
```
//@version=3
//study(title="Support Resistance Fibonacci Levels",shorttitle="SupResFib",overlay=true)
strategy("test",overlay=true)

use_monthly = input(true,"Use Monthly Basis?")
months_in = input(1,"# Months for Monthly Basis (this is limited by max lookback at your resolution!)",minval=1)
use_weekly = input(true,"Use Weekly Basis?")
weeks_in = input(2,"# Weeks for Weekly Basis",minval=1)
use_daily = input(true,"Use Daily Basis?")
days_in = input(7,"# Days for Daily Basis",minval=1)
use_hourly = input(false,"Use Hourly Basis? (only works on intraday charts)")
hours_in = input(84,"# Hours for Hourly Basis",minval=1)
use_minutes = input(false,"Use per-Minute Basis? (only works on intraday charts)")
minutes_in = input(2520,"# Minutes for per-Minute Basis",minval=1)
hold_intraday = input(true,"Keep Lines Straight All Day? (holds morning value until next day)")
connect_lines = input(false,"Connect Sup/Res Crosses with a Line?")
zigzag_depth = input(50,"Zig Zag Depth",minval=4)
zigzag_filter = input(0.0,"Zig Zag Reversal Sensitivity, %",minval=-99,maxval=99)

profitF382 = input(true,"F382")
profitF500 = input(false,"F500")
profitF618 = input(false,"F618")
profitF786 = input(false,"F786")
profitF1618 = input(false,"F1618")

zigzag(_price,_high,_low,_depth,_filter) =>
    _offset = max(floor(_depth/2),2)
    _deviation = highest(stdev(_price[_offset],50)/nz(_price[_offset],1),100)*(1-_filter/100)
    _window = na
    _window := nz(_window[1])+1
    _long_enough = _window>=_depth
    _top = _high[_offset-1]<nz(_high[_offset])
    _bottom = _low[_offset-1]>nz(_low[_offset])
    _zz_candidate = (_top or _bottom) and _long_enough
    _last_extreme = _high[_offset]
    _last_extreme := nz(_last_extreme[1])
    if _top and _bottom
        if abs(_last_extreme-_high[_offset])>abs(_last_extreme-_low[_offset])
            _bottom := false
        else
            _top := false
    _this_extreme = _top?_high[_offset]:(_bottom?_low[_offset]:na)
    _zz_found = (_zz_candidate and _long_enough and (abs(_this_extreme-_last_extreme)/nz(_price[_offset],1))>_deviation)
      or _high[_offset] >= highest(_high,_depth)
      or _low[_offset] <= lowest(_low,_depth)
    if _zz_found
        _last_extreme := _this_extreme
        _window := 0
    _zigzag = _zz_found?_this_extreme:na


zz = zigzag(hl2,high,low,zigzag_depth,zigzag_filter)
plot(zz,color=blue,linewidth=2,offset=-max(floor(zigzag_depth/2),2))

fib_source = input(title="Draw Fibonacci Levels For Which Basis Pair?",defval="Hourly",options=["None","Monthly","Weekly","Daily","Hourly","Minutes"])
fib_invert = input(false,"Invert Fibonacci Levels? (i.e. draw from max to min)")


daysinweek = input(5,"# Days in a Week of Trading (change to 7 for crypto)")

months = months_in%12
year_months =  floor(months_in/12)*12 //*12 added after commenting out below
//it really bugs me that highest() and lowest() cannot accept series inputs...
// monthly_bar = (month>(months+1))?(month-months_in):(month+12-months_in)
// yearly_bar = year - year_months
// monthly_high = highest(high,barssince(month==monthly_bar and year==yearly_bar))
// monthly_low = lowest(low,barssince(month==monthly_bar and year==yearly_bar))
monthly_bars = (ismonthly?months_in:((isintraday?1440:1)*(year_months*52*daysinweek+months*4*daysinweek)/(isweekly?daysinweek:1)))/interval
monthly_high = highest(high,min(5000,monthly_bars))
monthly_low = lowest(low,min(5000,monthly_bars))
//plot(n)
weeks = weeks_in%52
year_weeks = floor(weeks_in/52)*daysinweek
weekly_bars = (isweekly?weeks_in:((isintraday?1440:1)*(year_weeks*52*daysinweek+weeks*daysinweek)/(ismonthly?4*daysinweek:1)))/interval
weekly_high = highest(high,min(5000,weekly_bars))
weekly_low = lowest(low,min(5000,weekly_bars))

days = ismonthly?max(days_in,4*daysinweek):isweekly?max(days_in,daysinweek):days_in
daily_bars = (isdaily?days:((isintraday?1440:1)*days/((ismonthly?4*daysinweek:1)*(isweekly?daysinweek:1))))/interval
daily_high = highest(high,min(5000,daily_bars))
daily_low = lowest(low,min(5000,daily_bars))

//# hours per day varies widely...restrict these next two to intraday.
hours = isdwm?na:hours_in
hourly_bars = max(hours*60/interval,1)
hourly_high = highest(high,min(5000,isdwm?24:hourly_bars))
hourly_low = lowest(low,min(5000,isdwm?24:hourly_bars))

minutes = isdwm?na:minutes_in
minute_bars = max(minutes/interval,1)
minute_high = highest(high,min(5000,isdwm?1440:minute_bars))
minute_low = lowest(low,min(5000,isdwm?1440:minute_bars))

flat_monthly_high = high
flat_monthly_low = low
flat_weekly_high = high
flat_weekly_low = low
flat_daily_high = high
flat_daily_low = low

flat_monthly_high := change(dayofmonth)?monthly_high:na(monthly_high)?na:nz(flat_monthly_high[1],na(monthly_high[1])?monthly_high:high)
flat_monthly_low := change(dayofmonth)?monthly_low:na(monthly_low)?na:nz(flat_monthly_low[1],na(monthly_low[1])?monthly_low:low)
flat_weekly_high := change(dayofmonth)?weekly_high:na(weekly_high)?na:nz(flat_weekly_high[1],na(weekly_high[1])?weekly_high:high)
flat_weekly_low := change(dayofmonth)?weekly_low:na(weekly_low)?na:nz(flat_weekly_low[1],na(weekly_low[1])?weekly_low:low)
flat_daily_high := change(dayofmonth)?daily_high:na(daily_high)?na:nz(flat_daily_high[1],na(daily_high[1])?daily_high:high)
flat_daily_low := change(dayofmonth)?daily_low:na(daily_low)?na:nz(flat_daily_low[1],na(daily_low[1])?daily_low:low)

plot_monthly_high = hold_intraday?flat_monthly_high:monthly_high
plot_monthly_low = hold_intraday?flat_monthly_low:monthly_low
plot_weekly_high = hold_intraday?flat_weekly_high:weekly_high
plot_weekly_low = hold_intraday?flat_weekly_low:weekly_low
plot_daily_high = hold_intraday?flat_daily_high:daily_high
plot_daily_low = hold_intraday?flat_daily_low:daily_low

fhigh = na
flow = na

if fib_source!= "None"
    fhigh := fib_source=="Monthly"?plot_monthly_high:
      fib_source=="Weekly"?plot_weekly_high:
      fib_source=="Daily"?plot_daily_high:
      fib_source=="Hourly"?hourly_high:
      fib_source=="Minutes"?minute_high:
      na
    flow := fib_source=="Monthly"?plot_monthly_low:
      fib_source=="Weekly"?plot_weekly_low:
      fib_source=="Daily"?plot_daily_low:
      fib_source=="Hourly"?hourly_low:
      fib_source=="Minutes"?minute_low:
      na

calc_fib(_max,_min,_dir) =>
    _diff = _max-_min
    _236 = _diff*0.236*(_dir?-1:1)+(_dir?_max:_min)
    _382 = _diff*0.382*(_dir?-1:1)+(_dir?_max:_min)
    _500 = _diff*0.5*(_dir?-1:1)+(_dir?_max:_min)
    _618 = _diff*0.618*(_dir?-1:1)+(_dir?_max:_min)
    _786 = _diff*0.786*(_dir?-1:1)+(_dir?_max:_min)
    _1618 = _diff*1.618*(_dir?-1:1)+(_dir?_max:_min)
    [_236,_382,_500,_618,_786,_1618]

[F236,F382,F500,F618,F786,F1618] = calc_fib(fhigh,flow,fib_invert)
p236 = plot(F236,color=red)
p382 = plot(F382,color=yellow)
p500 = plot(F500,color=lime)
p618 = plot(F618,color=aqua)
p786 = plot(F786,color=teal)
p1618 = plot(F1618,color=blue)

color_monthly_high = use_monthly?gray:na
color_monthly_low = use_monthly?gray:na
pMh = plot(use_monthly?plot_monthly_high:high,style=cross,color=color_monthly_high,linewidth=4,transp=65,join=connect_lines) //change plotted values for better autoscaling
pMl = plot(use_monthly?plot_monthly_low:low,style=cross,color=color_monthly_low,linewidth=4,transp=80,join=connect_lines)

color_weekly_high = use_weekly?red:na
color_weekly_low = use_weekly?red:na
pwh = plot(use_weekly?plot_weekly_high:high,style=circles,color=color_weekly_high,linewidth=3,transp=65,join=connect_lines)
pwl = plot(use_weekly?plot_weekly_low:low,style=circles,color=color_weekly_low,linewidth=3,transp=80,join=connect_lines)

color_daily_high = use_daily?blue:na
color_daily_low = use_daily?blue:na
pdh = plot(use_daily?plot_daily_high:high,style=cross,color=color_daily_high,linewidth=3,transp=65,join=connect_lines)
pdl = plot(use_daily?plot_daily_low:low,style=cross,color=color_daily_low,linewidth=3,transp=80,join=connect_lines)

color_hourly_high = (isintraday and use_hourly)?green:na
color_hourly_low = (isintraday and use_hourly)?green:na
phh = plot((isintraday and use_hourly)?hourly_high:high,style=circles,color=color_hourly_high,linewidth=2,transp=65,join=connect_lines)
phl = plot((isintraday and use_hourly)?hourly_low:low,style=circles,color=color_hourly_low,linewidth=2,transp=80,join=connect_lines)

color_minute_high = (isintraday and use_minutes)?black:na
color_minute_low = (isintraday and use_minutes)?black:na
pmh = plot((isintraday and use_minutes)?minute_high:high,style=cross,color=color_minute_high,linewidth=2,transp=65,join=connect_lines)
pml = plot((isintraday and use_minutes)?minute_low:low,style=cross,color=color_minute_low,linewidth=2,transp=80,join=connect_lines)

// pzh=plot(high,color=na)
// pzl=plot(low,color=na)
fphigh=fib_invert?flow:fhigh
fplow=fib_invert?fhigh:flow
phigh = plot(fphigh,color=na)
plow = plot(fplow,color=na)
plot(close,"close")
plot(F786,"F786")


fill(plow,p236,red)
fill(p236,p382,yellow)
fill(p382,p500,lime)
fill(p500,p618,aqua)
fill(p618,p786,teal)
fill(p786,phigh,gray)
fill(phigh,p1618,blue)

longCondition = cross(close, F236)
shortCondition = cross(close, F618)
//plotshape(longCondition, style=shape.triangleup)

profit = profitF382 ? F382 : profitF500 ? F500 : profitF618 ? F618 : profitF786 ? F786 : profitF1618 ? F1618 : 0

if (strategy.position_size ==0 and longCondition)
    strategy.entry("Enter Long", strategy.long)
    strategy.exit("Exit Long", from_entry="Enter Long", profit=abs(F236-profit)*100, loss=abs(F236-fplow)*100)

if (strategy.position_size ==0 and shortCondition)
    strategy.entry("Enter Short", strategy.short)
    strategy.exit("Exit Short", from_entry="Enter Short", profit=abs(F618-F500)*100, loss=abs(F618-F786)*100)
```



